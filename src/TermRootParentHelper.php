<?php

namespace Drupal\taxonomy_term_root;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Provide a helper for managing term root relations.
 */
class TermRootParentHelper {

  /**
   * Update a given term entities root id property.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The target term.
   */
  public static function updateRootIds(TermInterface $term) {
    $roots = static::locateRoots($term);
    $root_ids = array_keys($roots);
    $term->get('root')->setValue($root_ids);
  }

  /**
   * Locate the parents of a given entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $item
   *   The entity to start discovery from.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface[]
   *   The root items of the passed entity.
   */
  protected static function locateRoots(FieldableEntityInterface $item): array {
    $roots = [];
    if ($item->hasField('parent') && !$item->get('parent')->isEmpty()) {
      /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $parents_field */
      $parents_field = $item->get('parent');
      $parent_ids = $parents_field->getValue();
      $parent_ids = array_column($parent_ids, 'target_id');

      if (in_array(0, $parent_ids)) {
        // 0 is the id for the <root> taxonomy term.
        $roots[$item->id()] = $item;
      }

      /** @var \Drupal\Core\Entity\FieldableEntityInterface[] $parent_instances */
      $parent_instances = $parents_field->referencedEntities();
      foreach ($parent_instances as $parent) {
        $roots += static::locateRoots($parent);
      }
    }
    else {
      // If an item doesn't have parents, it must be a root.
      $roots[$item->id()] = $item;
    }

    return $roots;
  }

}
