# Taxonomy Term Root

This module provides a computed field on taxonomy terms which tracks the highest
parent of a term in the taxonomy hierarchy tree. This allows for complex term
assignment, but also the ability to simply the list for end users.

## Functionality

This module provides an additional base field on all taxonomy terms which tracks
the highest level parent term in the tree. This is used to identify the root
term and use the root term as a relationship in view and in custom queries,
without needing to construct complex queries which may not be able to handle
the dynamic depths of taxonomy trees.

## Installation

This module can be installed following the standard module installation process
found at the link below.

https://www.drupal.org/docs/extending-drupal/installing-modules

During module installation, the module will generate the root term relationships
to ensure that data is available for any existing terms.

## Support

If you have any issues with this module, or would like to request new features,
please open a support ticket for this project at
https://www.drupal.org/project/taxonomy_term_root
