<?php

/**
 * @file
 * Provides batch callbacks for taxonomy_term_root installation.
 */

use Drupal\taxonomy_term_root\TermRootParentHelper;

/**
 * Dummy operation function for the batch process.
 */
function taxonomy_term_root_install_batch_operation(&$context) {
  $storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
  if (!isset($context['sandbox']['progress'])) {
    $context['results']['processed'] = 0;

    $total_query = $storage->getQuery();
    $total_query->notExists('root');
    $total_query->count();
    $total_query->accessCheck(TRUE);
    $total = $total_query->execute();
    if ($total) {
      $context['sandbox']['total'] = $total_query->execute();
    }
  }
  $query = $storage->getQuery();
  $query->notExists('root');
  $query->accessCheck(TRUE);
  $query->range(0, 100);
  $term_ids = $query->execute();

  /** @var \Drupal\taxonomy\TermInterface[] $terms */
  if (!empty($term_ids)) {
    $terms = $storage->loadMultiple($term_ids);

    foreach ($terms as $term) {
      TermRootParentHelper::updateRootIds($term);
      $term->save();
    }

    $context['results']['processed'] += count($term_ids);
  }

  if (!empty($context['sandbox']['total']) && $context['results']['processed'] <= $context['sandbox']['total']) {
    $context['finished'] = (float) $context['results']['processed'] / (float) $context['sandbox']['total'];
  }
}

/**
 * Batch callback.
 *
 * @param bool $success
 *   The batch status.
 * @param array $results
 *   The batch results.
 * @param array $operations
 *   The batch operations.
 */
function taxonomy_term_root_install_batch_finished(bool $success, array $results, array $operations) {
  if ($success) {
    \Drupal::messenger()->addStatus(t('Generated Root Ids for %count terms', ['%count' => $results['processed']]));
  }
  else {
    \Drupal::messenger()->addError(t('There is an error while generating root term ids.'));
  }
}
